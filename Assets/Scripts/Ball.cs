﻿using UnityEngine;

public class Ball : MonoBehaviour
{
    private readonly TaskManager _tm = new TaskManager();

    private void Start ()
    {
        var go = getGo();
        if (go == null)
        {
            Debug.Log("Nothing to see here");
        }

        DoMyThing();
    }


    GameObject getGo()
    {
        return Time.time < 1000000 ? null : new GameObject();
    }


    private void DoMyThing()
    {
        // Just setting up some variables so the task constructors below are a little easier to read...
        var startPos = Camera.main.ViewportToWorldPoint(new Vector3(0, 0.5f, 10));
        var endPos = Camera.main.ViewportToWorldPoint(new Vector3(1, 0.5f, 10));
        var midPos = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 10));

        var startScale = Vector3.one;
        var endScale = startScale * 2;

        // Teleport to the left of the screen immediately...
        _tm.Do(new SetPos(gameObject, startPos))

            // Move to the middle over half a second...
            .Then(new Move(gameObject, startPos, midPos, 0.5f))

            // Then scale up for one quarter of a second
            .Then(new Scale(gameObject, startScale, endScale, 0.25f))

            // Then scale down for one quarter of a second
            .Then(new Scale(gameObject, endScale, startScale, 0.25f))

            // Then move off screen over half a second
            .Then(new Move(gameObject, midPos, endPos, 0.25f))

            // Then reset the whole thing
            .Then(new ActionTask(DoMyThing));
    }

    private void Update ()
    {
        _tm.Update();
	}

}
